﻿using System;
using System.Collections.Generic;
using System.Security.Cryptography;
using Swatter;

namespace SW_Checksum
{
    public class Checksum : ModuleBase
    {
        #region Enums
        private enum Type
        {
            MD5,
            SHA1,
            SHA256,
            SHA384,
            SHA512,
        }
        #endregion

        #region Properties
        public override Guid ID { get { return Guid.Parse("{C9CDA01C-9E97-457C-A979-4EA576B13049}"); } }
        public override string Name { get { return "Checksum Bot"; } }
        public override string Description { get { return "Generates checksums from provided strings."; } }
        public override string Version { get { return "1.0.0"; } }
        #endregion

        #region Functions
        public override void Initialize()
        {
            RegisterGlobalCommand(Type.MD5, "!md5");
            RegisterGlobalCommand(Type.SHA1, "!sha1");
            RegisterGlobalCommand(Type.SHA256, "!sha256");
            RegisterGlobalCommand(Type.SHA384, "!sha384");
            RegisterGlobalCommand(Type.SHA512, "!sha512");
        }
        public override void GlobalCommandRecieved(object ID, string Arguments, MessageData MessageData)
        {
            switch ((Type)ID)
            {
                case Type.MD5: SendMessage(MessageData.Target, Hashing.MD5Hash(Arguments).ToLower()); break;
                case Type.SHA1: SendMessage(MessageData.Target, Hashing.SHA1Hash(Arguments).ToLower()); break;
                case Type.SHA256: SendMessage(MessageData.Target, Hashing.SHA256Hash(Arguments).ToLower()); break;
                case Type.SHA384: SendMessage(MessageData.Target, Hashing.SHA384Hash(Arguments).ToLower()); break;
                case Type.SHA512: SendMessage(MessageData.Target, Hashing.SHA512Hash(Arguments).ToLower()); break;
                default: break;
            }
        }
        #endregion
    }
}
