﻿using System;
using System.Security.Cryptography;
using System.Text;

namespace SW_Checksum
{
    public static class Hashing
    {
        public static string MD5Hash(string StringtoHash)
        {
            using (var hasher = new MD5Cng())
            {
                return BitConverter.ToString(hasher.ComputeHash(Encoding.UTF8.GetBytes(StringtoHash))).Replace("-", "");
            }
        }
        public static string SHA1Hash(string StringtoHash)
        {
            using (var hasher = new SHA1Managed())
            {
                return BitConverter.ToString(hasher.ComputeHash(Encoding.UTF8.GetBytes(StringtoHash))).Replace("-", "");
            }
        }
        public static string SHA256Hash(string StringtoHash)
        {
            using (var hasher = new SHA256Managed())
            {
                return BitConverter.ToString(hasher.ComputeHash(Encoding.UTF8.GetBytes(StringtoHash))).Replace("-", "");
            }
        }
        public static string SHA384Hash(string StringtoHash)
        {
            using (var hasher = new SHA384Managed())
            {
                return BitConverter.ToString(hasher.ComputeHash(Encoding.UTF8.GetBytes(StringtoHash))).Replace("-", "");
            }
        }
        public static string SHA512Hash(string StringtoHash)
        {
            using (var hasher = new SHA512Managed())
            {
                return BitConverter.ToString(hasher.ComputeHash(Encoding.UTF8.GetBytes(StringtoHash))).Replace("-", "");
            }
        }
    }
}
