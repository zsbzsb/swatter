﻿using System;
using System.Collections.Generic;
using Swatter;

namespace SW_Chatter
{
    public class Chatter : ModuleBase
    {
        #region Variables
        
        #endregion

        #region Properties
        public override Guid ID { get { return Guid.Parse("{E6E9F479-FB63-40E2-9A10-B0EADF9FDDFB}"); } }
        public override string Name { get { return "Chatter Bot"; } }
        public override string Description { get { return "Relays commands to Cleverbot and returns the response in chat."; } }
        public override string Version { get { return "1.0.0"; } }
        #endregion

        #region Constructors
        public Chatter()
        {
        }
        #endregion

        #region Functions
        
        #endregion
    }
}
