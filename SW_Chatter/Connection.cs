﻿using System;
using System.Net;
using System.Web;
using System.Collections.Specialized;

namespace SW_Chatter
{
    public class Connection
    {
        #region Variables
        private HttpWebRequest _connection = null;

        #endregion

        #region Constructors/Destructors
        public Connection()
        {
            _connection = WebRequest.CreateHttp("www.cleverbot.com/webservicemin");
            _connection.Method = "POST";
        }
        #endregion

        #region Functions
        public string RelayMessage(string Message)
        {
            string encode = HttpUtility.UrlEncode(Message);
            _connection.Headers.Clear();
            _connection.ContentType = "application/x-www-form-urlencoded";
            _connection.ContentLength = encode.Length;
            
            return "";
        }
        #endregion
    }
}
