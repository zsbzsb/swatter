﻿using System;
using System.Reflection;

namespace SW_CSharp
{
    [Serializable]
    public class Job
    {
        #region Variables
        private Guid _id = Guid.NewGuid();
        private string _sender = "";
        private string _target = "";
        private string _code = "";
        private string _compiledcode = "";
        private bool _outputfinished = false;
        private string _output = "";
        #endregion

        #region Properties
        public Guid ID
        {
            get
            {
                return _id;
            }
        }
        public string Sender
        {
            get
            {
                return _sender;
            }
        }
        public string Target
        {
            get
            {
                return _target;
            }
        }
        public string Code
        {
            get
            {
                return _code;
            }
        }
        public string CompiledCode
        {
            get
            {
                return _compiledcode;
            }
            set
            {
                _compiledcode = value;
            }
        }
        public bool OutputFinished
        {
            get
            {
                return _outputfinished;
            }
            set
            {
                _outputfinished = value;
            }
        }
        public string Output
        {
            get
            {
                return _output;
            }
            set
            {
                _output = value;
            }
        }
        #endregion

        #region Constructors
        public Job(string Sender, string Target, string Code)
        {
            _sender = Sender;
            _target = Target;
            _code = Code;
        }
        #endregion
    }
}
