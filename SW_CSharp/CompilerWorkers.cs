﻿//using System;
//using System.Threading;
//using System.Collections.Generic;
//using System.Collections.Concurrent;

//namespace SW_CSharp
//{
//    public class CompilerWorkers
//    {
//        #region Variables
//        private ConcurrentQueue<Job> _queuedjobs = new ConcurrentQueue<Job>();
//        private ConcurrentQueue<Job> _finishedjobs = new ConcurrentQueue<Job>();
//        private List<Thread> _workers = new List<Thread>();
//        private Compiler _compiler = new Compiler();
//        private bool _run = true;
//        #endregion

//        #region Constructors
//        public CompilerWorkers(int Workers)
//        {
//            for (int i = 0; i < Workers; i++)
//            {
//                _workers.Add(new Thread(new ThreadStart(WorkerThreadRun)));
//                _workers[_workers.Count - 1].Start();
//            }
//        }
//        #endregion

//        #region Functions
//        private void WorkerThreadRun()
//        {
//            while (_run)
//            {
//                if (!_queuedjobs.IsEmpty)
//                {
//                    Job jb = null;
//                    if (_queuedjobs.TryDequeue(out jb))
//                    {
//                        try
//                        {
//                            jb.CompiledCode = _compiler.CompileCode(jb.Code, jb.ID.ToString());
//                        }
//                        catch (Exception e)
//                        {
//                            jb.OutputFinished = true;
//                            jb.Output = e.Message;
//                        }
//                        _finishedjobs.Enqueue(jb);
//                    }
//                }
//                else
//                {
//                    Thread.Sleep(20);
//                }
//            }
//        }
//        public void AddJob(Job Job)
//        {
//            _queuedjobs.Enqueue(Job);
//        }
//        public Job GetFinishedJob()
//        {
//            if (_finishedjobs.IsEmpty) return null;
//            Job jb = null;
//            if (_finishedjobs.TryDequeue(out jb)) return jb;
//            else return null;
//        }
//        #endregion
//    }
//}
