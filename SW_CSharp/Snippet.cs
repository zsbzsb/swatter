﻿using System;
using System.IO;
using System.Text;
using System.Reflection;
using System.Security;
using System.Security.Policy;
using System.Security.Permissions;

namespace SW_CSharp
{
    public class Snippet : MarshalByRefObject
    {
        #region Variables
        private AppDomain _sandboxeddomain = null;
        private StringBuilder _consoleoutput = new StringBuilder();
        private Assembly _compiledcode = null;
        private Compiler _compiler = new Compiler();
        #endregion

        #region Constructors
        public Snippet()
        {
            _sandboxeddomain = CreateDomain();
            Console.SetOut(new StringWriter(_consoleoutput));
        }
        #endregion

        #region Functions
        private static AppDomain CreateDomain()
        {
            PermissionSet pset = new PermissionSet(PermissionState.None);
            pset.AddPermission(new SecurityPermission(SecurityPermissionFlag.Execution));
            pset.AddPermission(new ReflectionPermission(PermissionState.Unrestricted));
            return AppDomain.CreateDomain("Sandboxed Domain", null, new AppDomainSetup() { ApplicationBase = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location) }, pset, CreateStrongName(Assembly.GetExecutingAssembly()));
        }
        private static StrongName CreateStrongName(Assembly assembly)
        {
            AssemblyName assemblyName = assembly.GetName();
            byte[] publicKey = assemblyName.GetPublicKey();
            StrongNamePublicKeyBlob keyBlob = new StrongNamePublicKeyBlob(publicKey);
            return new StrongName(keyBlob, assemblyName.Name, assemblyName.Version);
        }
        public bool CPULimitReached()
        {
            return _sandboxeddomain.MonitoringTotalProcessorTime.Seconds > 5;
        }
        public bool MemoryLimitReached()
        {
            return _sandboxeddomain.MonitoringTotalAllocatedMemorySize > 13110000;
        }
        public void UnloadDomain()
        {
            AppDomain.Unload(_sandboxeddomain);
        }
        public Job RunJob(Job Job)
        {
            try
            {
                _compiledcode = _compiler.CompileCode(Job.Code, Job.ID.ToString());
            }
            catch (Exception e)
            {
                Job.Output = "Error: " + e.Message;
                return Job;
            }
            var handle = Activator.CreateInstanceFrom(_sandboxeddomain, typeof(Executable).Assembly.ManifestModule.FullyQualifiedName, typeof(Executable).FullName);
            Executable executable = (Executable)handle.Unwrap();
            Console.WriteLine(executable.Run(_compiledcode));
            Job.Output = _consoleoutput.ToString();
            return Job;
        }
        #endregion
    }
}
