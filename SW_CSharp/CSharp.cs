﻿using System;
using System.IO;
using System.Collections.Generic;
using Swatter;

namespace SW_CSharp
{
    public class CSharp : ModuleBase
    {
        #region Enums
        private enum Language
        {
            CSharp
        }
        #endregion

        #region Variables
        private List<SnippetManager> _runningcode = new List<SnippetManager>();
        private List<SnippetManager> _codequeue = new List<SnippetManager>();
        private List<Guid> _deletelist = new List<Guid>();
        #endregion

        #region Properties
        public override Guid ID { get { return Guid.Parse("{361A11EF-B4B9-4BA9-A790-A970941AD74D}"); } }
        public override string Name { get { return "CSharp Evaluation"; } }
        public override string Description { get { return "Evaluates and returns the output of CSharp programs."; } }
        public override string Version { get { return "1.0.0"; } }
        #endregion

        #region Constructors
        #endregion

        #region Functions
        public override void Initialize()
        {
            RegisterGlobalCommand(Language.CSharp, "c#");
            AppDomain.MonitoringIsEnabled = true;
        }
        public override void GlobalCommandRecieved(object ID, string Arguments, MessageData MessageData)
        {
            if ((Language)ID == Language.CSharp)
            {
                var jb = new Job(MessageData.Source, MessageData.Target, Arguments);
                var snip = new SnippetManager(jb);
                _codequeue.Add(snip);
            }
        }
        public override void Update(Time DeltaTime)
        {
            List<SnippetManager> removelist = null;
            foreach (var code in _runningcode)
            {
                if (code.CurrentJob.OutputFinished)
                {
                    code.UnloadDomain();
                    GC.Collect();
                    _deletelist.Add(code.CurrentJob.ID);
                    SendMessage(code.CurrentJob.Target, code.CurrentJob.Sender + ": " + code.CurrentJob.Output);
                    if (removelist == null) removelist = new List<SnippetManager>();
                    removelist.Add(code);
                }
                else
                {
                    code.Update(DeltaTime);
                }
            }
            if (removelist != null)
            {
                foreach (var code in removelist)
                {
                    _runningcode.Remove(code);
                }
            }
            while (_codequeue.Count > 0 && _runningcode.Count < 3)
            {
                _runningcode.Add(_codequeue[0]);
                _codequeue[0].HandleJob();
                _codequeue.RemoveAt(0);
            }
            for (int i = 0; i < _deletelist.Count; )
            {
                if (File.Exists(_deletelist[i].ToString() + ".dll"))
                {
                    try
                    {
                        File.Delete(_deletelist[i].ToString() + ".dll");
                    }
                    catch { }
                    i++;
                }
                else
                {
                    _deletelist.RemoveAt(i);
                }
            }
        }
        #endregion
    }
}
