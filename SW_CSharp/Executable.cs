﻿using System;
using System.Text;
using System.IO;
using System.Security;
using System.Reflection;

namespace SW_CSharp
{
    public class Executable : MarshalByRefObject
    {
        #region Functions
        public string Run(Assembly CompiledCode)
        {
            StringBuilder output = new StringBuilder ();
            // Hack to change the console output without actual permission to do so
            typeof(Console).GetField("_out", BindingFlags.NonPublic | BindingFlags.Static).SetValue(null, new StringWriter(output));
            // Do not try the above at home kids :)
            try
            {
                bool foundentry = false;
                foreach (var type in CompiledCode.GetTypes())
                {
                    foreach (var method in type.GetMethods(BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Static))
                    {
                        if (method.Name.ToLower() == "main")
                        {
                            method.Invoke(null, new object[method.GetParameters().Length]);
                            foundentry = true;
                        }
                    }
                }
                if (!foundentry) Console.WriteLine("Error: No Suitable Entry Point");
            }
            catch (Exception e)
            {
                if (e.InnerException != null)
                {
                    if (e.InnerException.GetType() == typeof(SecurityException))
                    {
                        Console.WriteLine("Terminated: Restricted Function Used");
                    }
                    else
                    {
                        Console.WriteLine("Terminated: Unhandled Exception - " + e.InnerException.Message);
                    }
                }
                else
                {
                    Console.WriteLine("Terminated: Unhandled Exception - " + e.Message);
                }
            }
            return output.ToString();
        }
        #endregion
    }
}
