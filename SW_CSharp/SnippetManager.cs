﻿using System;
using System.IO;
using System.Threading;
using System.Reflection;
using Swatter;

namespace SW_CSharp
{
    public class SnippetManager
    {
        #region Variables
        private AppDomain _wrapperdomain = null;
        private Job _job = null;
        private Snippet _snippet = null;
        private Thread _codethread = null;
        private Time _elaspedtime = Time.Zero;
        #endregion

        #region Properties
        public Job CurrentJob
        {
            get
            {
                return _job;
            }
        }
        #endregion

        #region Constructors
        public SnippetManager(Job Job)
        {
            _job = Job;
            _wrapperdomain = AppDomain.CreateDomain("Sandbox Wrapper");
            var handle = Activator.CreateInstanceFrom(_wrapperdomain, typeof(Snippet).Assembly.ManifestModule.FullyQualifiedName, typeof(Snippet).FullName);
            _snippet = (Snippet)handle.Unwrap();
            _codethread = new Thread(new ThreadStart(CodeThreadEntry));
        }
        #endregion

        #region Functions
        public void UnloadDomain()
        {
            _snippet.UnloadDomain();
            _snippet = null;
            AppDomain.Unload(_wrapperdomain);
        }
        public void Update(Time DeltaTime)
        {
            _elaspedtime += DeltaTime;
            if (_elaspedtime >= Time.FromSeconds(10))
            {
                _job.Output = "Terminated: Execution Timeout";
                _job.OutputFinished = true;
            }
            else if (_snippet.MemoryLimitReached())
            {
                _job.Output = "Terminated: Memory Limit Reached";
                _job.OutputFinished = true;
            }
            else if (_snippet.CPULimitReached())
            {
                _job.Output = "Terminated: CPU Time Limit Reached";
                _job.OutputFinished = true;
            }
        }
        public void HandleJob()
        {
            _codethread.Start();
        }
        private void CodeThreadEntry()
        {
            _job = _snippet.RunJob(_job);
            _job.Output = CleanString(_job.Output);
            _job.OutputFinished = true;
        }
        private string CleanString(string String)
        {
            String = String.Replace("\r", "");
            String = String.Replace("\f", "");
            while (String.EndsWith("\n"))
            {
                String = String.Substring(0, String.Length - 1);
            }
            int lines = String.Split('\n').Length;
            String = String.Split('\n')[0];
            if (String.Length > 400)
            {
                int length = String.Length;
                String = String.Substring(0, 400);
                String += " <+" + (length - String.Length) + " Deletions(s)>";
            }
            else if (lines > 1) String += " <+" + (lines - 1) + " Line(s)>";
            return String;
        }
        #endregion
    }
}
