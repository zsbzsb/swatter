﻿using System;
using System.IO;
using System.Reflection;
using System.CodeDom.Compiler;
using System.Collections.Generic;
using Microsoft.CSharp;

namespace SW_CSharp
{
    public class Compiler
    {
        public Assembly CompileCode(string Code, string ID)
        {
            Code = ProcessCode(Code);
            CSharpCodeProvider cscp = new CSharpCodeProvider();
            CompilerParameters param = new CompilerParameters();
            param.GenerateExecutable = false;
            param.GenerateInMemory = false;
            param.WarningLevel = 0;
            param.ReferencedAssemblies.Add("mscorlib.dll");
            param.ReferencedAssemblies.Add("System.dll");
            param.ReferencedAssemblies.Add("System.Core.dll");
            param.OutputAssembly = ID + ".dll";
            CompilerResults results = cscp.CompileAssemblyFromSource(param, Code);
            if (results.Errors.Count == 0)
            {
                return results.CompiledAssembly;
            }
            else
            {
                throw new Exception(results.Errors[0].ErrorText);
            }
        }
        private string ProcessCode(string Code)
        {
            if (!Code.ToLower().Contains("static void main")) Code = "static void main() { " + Code + " }";
            if (!Code.Contains("static class")) Code = "static class ADDEDPRGENTRY { " + Code + " }";
            if (!Code.Contains("using System;")) Code = "using System;" + Code;
            if (!Code.Contains("using System.IO;")) Code = "using System.IO;" + Code;
            if (!Code.Contains("using System.Collections.Generic;")) Code = "using System.Collections.Generic;" + Code;
            if (!Code.Contains("using System.Linq;")) Code = "using System.Linq;" + Code;
            if (!Code.Contains("using System.Text;")) Code = "using System.Text;" + Code;
            if (!Code.Contains("using System.Threading;")) Code = "using System.Threading;" + Code;
            return Code;
        }
    }
}
