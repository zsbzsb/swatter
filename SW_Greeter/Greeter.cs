﻿using System;
using System.Collections.Generic;
using Swatter;

namespace SW_Greeter
{
    public class Greeter : ModuleBase
    {
        #region Variables
        float _delay = 5;
        bool _enablegreeting = true;
        #endregion

        #region Properties
        public override Guid ID { get { return Guid.Parse("{70726446-9302-4EDC-BAB1-E90352CA0170}"); } }
        public override string Name { get { return "Channel Greeter"; } }
        public override string Description { get { return "Greets users when they join a channel."; } }
        public override string Version { get { return "1.0.0"; } }
        #endregion

        #region Constructors
        public Greeter(object[] Flags)
        {
            foreach (var arg in Flags)
            {
                if (arg.GetType() == typeof(KeyValuePair<string, string>))
                {
                    var pair = (KeyValuePair<string, string>)arg;
                    switch (pair.Key.ToLower())
                    {
                        case "delay": { _delay = float.Parse(pair.Value); break; }
                        case "greetenabled": { _enablegreeting = bool.Parse(pair.Value); break; }
                    }
                }
            }
        }
        #endregion

        #region Functions
        public override void IRCCommandRecieved(CommandData CommandData)
        {
            if (CommandData.Command == Command.Join)
            {
                if (_enablegreeting)
                {
                    string user = CommandData.Source.Substring(0, CommandData.Source.IndexOf("!"));
                    string targ = CommandData.Arguments.Substring(1, CommandData.Arguments.Length - 1);
                    if (user.ToLower() != Nickname.ToLower()) RegisterCallback(new Action<string, string>(SendMessage), new object[] { targ, "Welcome " + user + "!" }, Time.FromSeconds(_delay), false);
                }
            }
        }
        #endregion
    }
}
