﻿using System;

namespace Swatter
{
    internal static class Entry
    {
        public static void Main(string[] args)
        {
            Console.WriteLine("Welcome to Swatter - An IRC Bot written by Zachariah Brown\r\n");
            Console.WriteLine("Initializing Controller...");
            try
            {
                new Controller().Start();
            }
            catch (Exception e)
            {
                Console.WriteLine("\r\n\r\nFatel unhandled exception:\r\n\r\n" + BreakoutException(e) + "\r\n\r\nPress 'enter' to exit.");
                Console.ReadLine();
                Environment.Exit(-1);
            }
        }
        private static string BreakoutException(Exception Exception, string Message = "")
        {
            string returnmessage = Message;
            if (Exception != null)
            {
                returnmessage += Exception.Message + " " + Exception.StackTrace + " ";
                if (Exception.InnerException != null) returnmessage = BreakoutException(Exception.InnerException, returnmessage);
            }
            return returnmessage.Trim();
        }
    }
}
