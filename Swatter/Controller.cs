﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Threading;

namespace Swatter
{
    internal class Controller
    {
        #region Variables
        private List<Tuple<string, object[]>> _globalmodulelist = new List<Tuple<string, object[]>>();
        private List<ServerManager> _servers = new List<ServerManager>();
        #endregion

        #region Functions
        public void Start()
        {
            Console.WriteLine("Controller is initialized.");
            Console.WriteLine("Begining Server List Load...");
            ConfigLoader loader = new ConfigLoader();
            loader.AddServer += AddServer;
            loader.AddChannel += AddChannel;
            loader.AddModule += AddModule;
            loader.LoadConfig();
            Console.WriteLine("Server List Load is complete.");
            Console.WriteLine("Loading all modules...");
            foreach (var serv in _servers)
            {
                serv.LoadLocalModules();
                foreach (var mod in _globalmodulelist)
                {
                    serv.LoadModule(mod.Item1, mod.Item2);
                }
            }
            Console.WriteLine("Module loading complete.");
            Console.WriteLine("Initializing all modules...");
            foreach (var serv in _servers)
            {
                serv.InitializeModules();
            }
            Console.WriteLine("Module initialization complete.");
            Console.WriteLine("Starting Server connections...");
            foreach (var serv in _servers)
            {
                serv.StartServer();
            }
            while (true)
            {
                Dispatcher.CurrentDispatcher.HandleCallbacks();
                Thread.Sleep(25);
            }
        }
        private void AddServer(string Host, int Port, string Nick, string Password)
        {
            if ((from svr in _servers where svr.Server.ToLower() == Host.ToLower() select svr).ToArray().Length > 0) return;
            _servers.Add(new ServerManager(Host, Port, Nick, Password));
        }
        private void AddChannel(string Target, string Channel)
        {
            if ((from svr in _servers where svr.Server.ToLower() == Target.ToLower() select svr).ToArray().Length == 0) return;
            (from svr in _servers where svr.Server.ToLower() == Target.ToLower() select svr).First().AddChannel(Channel);
        }
        private void AddModule(string Target, string Path, object[] Arguments)
        {
            if (Target != "")
            {
                if ((from svr in _servers where svr.Server.ToLower() == Target.ToLower() select svr).ToArray().Length == 0) return;
                (from svr in _servers where svr.Server.ToLower() == Target.ToLower() select svr).First().AddModule(Path, Arguments);
            }
            else _globalmodulelist.Add(new Tuple<string, object[]>(Path, Arguments));
        }
        #endregion
    }
}
