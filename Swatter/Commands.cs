﻿using System;

namespace Swatter
{
    public enum Command
    {
        Unknown,
        Ping,
        Message,
        Join,
        Part,
        Nick,
        Kick,
        Quit,
        ServerMOTD,
        EndServerMOTD,
        UserList,
        EndUserList,
        NoNickGiven,
        InvalidNick,
        NickAlreadyInUse,
        NickCollision,
        UserNotInChannel,
        NotOnChannel,
        UserOnChannel
    }
    public static class CommandParser
    {
        public static Command Parse(string CommandString)
        {
            switch (CommandString.ToUpper())
            {
                case "PING": return Command.Ping;
                case "PRIVMSG": return Command.Message;
                case "JOIN": return Command.Join;
                case "PART": return Command.Part;
                case "NICK": return Command.Nick;
                case "KICK": return Command.Kick;
                case "QUIT": return Command.Quit;
                case "372": return Command.ServerMOTD;
                case "376": return Command.EndServerMOTD;
                case "353": return Command.UserList;
                case "366": return Command.EndUserList;
                case "431": return Command.NoNickGiven;
                case "432": return Command.InvalidNick;
                case "433": return Command.NickAlreadyInUse;
                case "436": return Command.NickCollision;
                case "441": return Command.UserNotInChannel;
                case "442": return Command.NotOnChannel;
                case "443": return Command.UserOnChannel;
                default: return Command.Unknown;
            }
        }
        public static string Parse(Command CommandValue)
        {
            switch (CommandValue)
            {
                case Command.Ping: return "PING";
                case Command.Message: return "PRIVMSG";
                case Command.Join: return "JOIN";
                case Command.Part: return "PART";
                case Command.Nick: return "NICK";
                case Command.Kick: return "KICK";
                case Command.Quit: return "QUIT";
                case Command.ServerMOTD: return "372";
                case Command.EndServerMOTD: return "376";
                case Command.UserList: return "353";
                case Command.EndUserList: return "366";
                case Command.NoNickGiven: return "431";
                case Command.InvalidNick: return "432";
                case Command.NickAlreadyInUse: return "433";
                case Command.NickCollision: return "436";
                case Command.UserNotInChannel: return "441";
                case Command.NotOnChannel: return "442";
                case Command.UserOnChannel: return "443";
                default: return "";
            }
        }
    }
}
