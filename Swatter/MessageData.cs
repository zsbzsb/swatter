﻿using System;

namespace Swatter
{
    public class MessageData
    {
        #region Variables
        private string _source = "";
        private string _target = "";
        private string _message = "";
        #endregion

        #region Properties
        public string Source
        {
            get
            {
                return _source;
            }
        }
        public string Target
        {
            get
            {
                return _target;
            }
        }
        public string Message
        {
            get
            {
                return _message;
            }
        }
        #endregion

        #region Constructors
        public MessageData(string Source, string Target, string Message)
        {
            _source = Source;
            _target = Target;
            _message = Message;
        }
        #endregion
    }
}
