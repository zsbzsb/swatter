﻿using System;
using System.Collections.Generic;

namespace Swatter
{
    internal class ModuleManager
    {
        #region Variables
        private List<ModuleBase> _modules = new List<ModuleBase>();
        private List<Tuple<Delegate, object[], Time>> _callbacklist = new List<Tuple<Delegate, object[], Time>>();
        private List<Time> _callbacktimelist = new List<Time>();
        private Dictionary<string, List<Tuple<ModuleBase, object>>> _globalcommands = new Dictionary<string, List<Tuple<ModuleBase, object>>>();
        private Dictionary<string, List<Tuple<ModuleBase, object>>> _nickcommands = new Dictionary<string, List<Tuple<ModuleBase, object>>>();
        private string _nickname = "";
        #endregion

        #region Events
        public event Action<string, string> SendMessage;
        public event Action<string> SendCommand;
        #endregion

        #region Constructors
        public ModuleManager(string Nickname)
        {
            _nickname = Nickname;
        }
        #endregion

        #region Functions
        public void RegisterModule(ModuleBase Module)
        {
            _modules.Add(Module);
        }
        public void InitializeModules()
        {
            foreach (var mod in _modules)
            {
                mod.Nickname = _nickname;
                mod.SetManager(this);
                mod.Initialize();
            }
        }
        public void Update(Time DeltaTime)
        {
            foreach (var mod in _modules)
            {
                mod.Update(DeltaTime);
            }
            for (int i = 0; i < _callbacktimelist.Count; )
            {
                _callbacktimelist[i] -= DeltaTime;
                if (_callbacktimelist[i] <= Time.Zero)
                {
                    _callbacklist[i].Item1.DynamicInvoke(_callbacklist[i].Item2);
                    if (_callbacklist[i].Item3 != null)
                    {
                        _callbacktimelist[i] = Time.FromTicks(_callbacklist[i].Item3.Ticks);
                        i++;
                    }
                    else
                    {
                        _callbacklist.RemoveAt(i);
                        _callbacktimelist.RemoveAt(i);
                    }
                }
                else i++;
            }
        }
        public void MessageRecieved(MessageData MessageData)
        {
            foreach (var mod in _modules)
            {
                mod.MessageRecieved(MessageData);
            }
            {
                var splits = MessageData.Message.Split(" ".ToCharArray(), 2);
                if (splits.Length < 1) return;
                string cmd = splits[0];
                string arg = (splits.Length == 2 ? splits[1] : "");
                if (_globalcommands.ContainsKey(cmd.ToLower()))
                {
                    foreach (var call in _globalcommands[cmd.ToLower()])
                    {
                        call.Item1.GlobalCommandRecieved(call.Item2, arg, MessageData);
                    }
                }
            }
            {
                string nick = "";
                string cmd = "";
                string arg = "";
                if (MessageData.Target.StartsWith("#"))
                {
                    var splits = MessageData.Message.Split(" ".ToCharArray(), 3);
                    if (splits.Length < 2) return;
                    nick = splits[0];
                    cmd = splits[1];
                    arg = (splits.Length == 3 ? splits[2] : "");
                }
                else
                {
                    if (!MessageData.Message.ToLower().StartsWith(_nickname.ToLower()))
                    {
                        var splits = MessageData.Message.Split(" ".ToCharArray(), 2);
                        if (splits.Length < 1) return;
                        nick = _nickname;
                        cmd = splits[0];
                        arg = (splits.Length == 2 ? splits[1] : "");
                    }
                    else
                    {
                        var splits = MessageData.Message.Split(" ".ToCharArray(), 3);
                        if (splits.Length < 2) return;
                        nick = splits[0];
                        cmd = splits[1];
                        arg = (splits.Length == 3 ? splits[2] : "");
                    }
                }
                if (_nickcommands.ContainsKey(cmd.ToLower()) && nick.ToLower().StartsWith(_nickname.ToLower()))
                {
                    foreach (var call in _nickcommands[cmd.ToLower()])
                    {
                        call.Item1.NickCommandRecieved(call.Item2, arg, MessageData);
                    }
                }
            }
        }
        public void CommandRecieved(CommandData CommandData)
        {
            foreach (var mod in _modules)
            {
                mod.IRCCommandRecieved(CommandData);
            }
        }
        public void OnSendCommand(ModuleBase Sender, string Command)
        {
            if (SendCommand != null) SendCommand(Command);
        }
        public void OnSendMessage(ModuleBase Sender, string Target, string Message)
        {
            if (SendMessage != null) SendMessage(Target, Message);
        }
        public void OnRegisterCallback(ModuleBase Sender, Delegate Callback, object[] Parameters, Time TimeLimit, bool Loop)
        {
            _callbacklist.Add(new Tuple<Delegate, object[], Time>(Callback, Parameters, Loop ? TimeLimit : null));
            _callbacktimelist.Add(TimeLimit);
        }
        public void OnRegisterGlobalCommand(ModuleBase Sender, object ID, string Command)
        {
            if (!_globalcommands.ContainsKey(Command.ToLower())) _globalcommands.Add(Command.ToLower(), new List<Tuple<ModuleBase, object>>());
            _globalcommands[Command.ToLower()].Add(new Tuple<ModuleBase, object>(Sender, ID));
        }
        public void OnRegisterNickCommand(ModuleBase Sender, object ID, string Command)
        {
            if (!_nickcommands.ContainsKey(Command.ToLower())) _nickcommands.Add(Command.ToLower(), new List<Tuple<ModuleBase, object>>());
            _nickcommands[Command.ToLower()].Add(new Tuple<ModuleBase, object>(Sender, ID));
        }
        #endregion
    }
}
