﻿using System;

namespace Swatter
{
    public class CommandData
    {
        #region Variables
        private string _source = "";
        private Command _command = Command.Unknown;
        private string _arguments = "";
        #endregion

        #region Properties
        public string Source
        {
            get
            {
                return _source;
            }
        }
        public Command Command
        {
            get
            {
                return _command;
            }
        }
        public string Arguments
        {
            get
            {
                return _arguments;
            }
        }
        #endregion

        #region Constructors
        public CommandData(string Source, Command Command, string Arguments)
        {
            _source = Source;
            _command = Command;
            _arguments = Arguments;
        }
        #endregion
    }
}
