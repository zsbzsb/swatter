﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Net.Sockets;
using System.Threading;

namespace Swatter
{
    internal class IRCClient
    {
        #region Variables
        private string _server = "";
        private int _port = 6667;
        private string _nick = "";
        private string _password = "";
        private List<string> _channels = new List<string>();
        private Dictionary<string, List<string>> _users = new Dictionary<string, List<string>>();
        private bool _connected = false;
        private bool _authenticated = false;
        private bool _ready = false;
        private string _serverMOTD = "";
        private TcpClient _connection = null;
        private IRCStreamReader _reader = null;
        private StreamWriter _writer = null;
        #endregion

        #region Properties
        public string Server
        {
            get
            {
                return _server;
            }
        }
        public int Port
        {
            get
            {
                return _port;
            }
        }
        public string Nick
        {
            get
            {
                return _nick;
            }
        }
        public string Password
        {
            get
            {
                return _password;
            }
        }
        public bool IsReady
        {
            get
            {
                return _ready;
            }
        }
        #endregion

        #region Events
        public event Action<IRCClient, string> RecievedServerMOTD;
        public event Action<IRCClient> ReadyForCommands;
        public event Action<IRCClient, string, string[]> UserListRecieved;
        public event Action<IRCClient, MessageData> MessageRecieved;
        public event Action<IRCClient, CommandData> CommandRecieved;
        public event Action<IRCClient> NickAlreadyInUse;
        public event Action<IRCClient, Time> Update;
        #endregion

        #region Constructors
        public IRCClient(string Server, string Nick, string Password = "", int Port = 6667)
        {
            _server = Server;
            _port = Port;
            _nick = Nick;
            _password = Password;
        }
        #endregion

        #region Functions
        public void RegisterChannel(string Channel)
        {
            if (!Channel.StartsWith("#")) return;
            if (!_channels.Contains(Channel.ToLower())) _channels.Add(Channel.ToLower());
            Join(Channel);
        }
        public void UnregisterChannel(string Channel)
        {
            if (_channels.Contains(Channel)) _channels.Remove(Channel);
            Leave(Channel);
        }
        public bool Connect()
        {
            _connection = new TcpClient();
            try
            {
                _connection.Connect(_server, _port);
            }
            catch
            {
                _connection = null;
                return false;
            }
            _reader = new IRCStreamReader(_connection.GetStream());
            _writer = new StreamWriter(_connection.GetStream());
            _connected = true;
            return true;
        }
        public void Run()
        {
            Clock frameclock = new Clock();
            while (_connected)
            {
                foreach (string line in _reader.PollCommands())
                {
                    Authenticate();
                    string cmd = "";
                    string source = "";
                    string arguments = "";
                    if (line.StartsWith(":"))
                    {
                        string[] splits = line.Split(" ".ToCharArray(), 3);
                        source = splits[0].Substring(1, splits[0].Length - 1);
                        cmd = splits[1];
                        arguments = splits[2];
                    }
                    else
                    {
                        string[] splits = line.Split(" ".ToCharArray(), 2);
                        cmd = splits[0];
                        arguments = splits[1];
                    }
                    Command command = CommandParser.Parse(cmd);
                    switch (command)
                    {
                        case Command.Ping:
                            {
                                SendCommand("PONG " + arguments);
                                break;
                            }
                        case Command.ServerMOTD:
                            {
                                _serverMOTD += _serverMOTD.Length > 0 ? "\n" : "";
                                _serverMOTD += arguments;
                                break;
                            }
                        case Command.EndServerMOTD:
                            {
                                _ready = true;
                                JoinAll();
                                if (ReadyForCommands != null) ReadyForCommands(this);
                                if (RecievedServerMOTD != null) RecievedServerMOTD(this, _serverMOTD);
                                break;
                            }
                        case Command.UserList:
                            {
                                string[] splits = arguments.Trim().Split(" ".ToCharArray());
                                string channel = splits[2];
                                string user = splits[splits.Length - 1].Replace("+", "").Replace("@", "").Replace(":", "");
                                if (!_users.ContainsKey(channel)) _users.Add(channel, new List<string>());
                                if (!_users[channel].Contains(user)) _users[channel].Add(user);
                                break;
                            }
                        case Command.EndUserList:
                            {
                                string[] splits = arguments.Trim().Split(" ".ToCharArray());
                                string channel = (from arg in splits where arg.StartsWith("#") select arg).FirstOrDefault();
                                if (channel != "" && UserListRecieved != null) UserListRecieved(this, channel, _users[channel].ToArray());
                                break;
                            }
                        case Command.Message:
                            {
                                if (MessageRecieved != null)
                                {
                                    string[] splits = arguments.Trim().Split(" ".ToCharArray(), 2);
                                    MessageRecieved(this, new MessageData(source.Substring(0, source.IndexOf("!")), (splits[0].StartsWith("#") ? splits[0] : source.Substring(0, source.IndexOf("!"))), splits[1].Substring(1, splits[1].Length - 1)));
                                }
                                break;
                            }
                        case Command.NickAlreadyInUse:
                            {
                                _authenticated = false;
                                if (NickAlreadyInUse != null) NickAlreadyInUse(this);
                                break;
                            }
                        default: break;
                    }
                    if (CommandRecieved != null) CommandRecieved(this, new CommandData(source, command, arguments));
                }
                if (Update != null) Update(this, frameclock.Restart());
                else frameclock.Restart();
                Thread.Sleep(15);
            }
            _connected = false;
            _authenticated = false;
            _ready = false;
            _reader = null;
            _writer.Dispose();
            _writer = null;
            _connection = null;
        }
        private void Authenticate()
        {
            if (_authenticated) return;
            _authenticated = true;
            SendCommand("USER " + _nick + " 0 * :" + _nick);
            SendCommand("NICK " + _nick);
            if (_password != "") SendMessage("NickServ", "IDENTIFY " + _password);
        }
        private void JoinAll()
        {
            foreach (var chan in _channels)
            {
                Join(chan);
            }
        }
        private void Join(string Channel)
        {
            if (!_ready) return;
            SendCommand("JOIN " + Channel);
        }
        private void LeaveAll()
        {
            foreach (var chan in _channels)
            {
                Leave(chan);
            }
        }
        private void Leave(string Channel)
        {
            if (!_ready) return;
            SendCommand("PART " + Channel);
        }
        public void SendCommand(string Command)
        {
            _writer.WriteLine(Command);
            _writer.Flush();
        }
        public void SendMessage(string Target, string Message)
        {
            SendCommand("PRIVMSG " + Target + " :" + Message);
        }
        #endregion
    }
}
