﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Reflection;

namespace Swatter
{
    internal static class ModuleLoader
    {
        #region Variables
        private static Dictionary<string, List<Type>> _loadedmoduletypes = new Dictionary<string, List<Type>>();
        #endregion

        #region Functions
        public static ModuleBase[] LoadModule(string Path, object[] Arguments)
        {
            if (!_loadedmoduletypes.ContainsKey(Path))
            {
                Assembly loader = Assembly.LoadFrom(Path);
                Type[] typelist = loader.GetTypes();
                foreach (Type type in typelist)
                {
                    if (typeof(ModuleBase) == type.BaseType)
                    {
                        if (!_loadedmoduletypes.ContainsKey(Path)) _loadedmoduletypes.Add(Path, new List<Type>());
                        _loadedmoduletypes[Path].Add(type);
                    }
                }
            }
            if (!_loadedmoduletypes.ContainsKey(Path)) throw new Exception("Unable to load any modules for '" + Path + "'");
            List<ModuleBase> instances = new List<ModuleBase>();
            foreach (var mod in _loadedmoduletypes[Path])
            {
                instances.Add((ModuleBase)Activator.CreateInstance(mod, Arguments));
            }
            return instances.ToArray();
        }
        #endregion
    }
}
