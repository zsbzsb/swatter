﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml;

namespace Swatter
{
    internal class ConfigLoader
    {
        #region Events
        public event Action<string, int, string, string> AddServer;
        public event Action<string, string> AddChannel;
        public event Action<string, string, object[]> AddModule;
        #endregion

        #region Functions
        public void LoadConfig()
        {
            XmlDocument doc = new XmlDocument();
            doc.Load("swatter.cnf");
            var modules = (from XmlNode node in
                               (from XmlNode node in
                                    (from XmlNode node in doc.ChildNodes
                                     where node.NodeType == XmlNodeType.Element && node.Name.ToLower() == "swatter"
                                     select node).First().ChildNodes
                                where node.NodeType == XmlNodeType.Element && node.Name.ToLower() == "modules"
                                select node).First().ChildNodes
                           where node.NodeType == XmlNodeType.Element && node.Name.ToLower() == "module"
                           select node).ToArray();
            foreach (XmlNode modulenode in modules)
            {
                string path = "";
                List<object> arguments = new List<object>();
                foreach (XmlNode childnode in (from XmlNode node in modulenode.ChildNodes where node.NodeType == XmlNodeType.Element select node).ToArray())
                {
                    switch (childnode.Name.ToLower())
                    {
                        case "path": { path = childnode.InnerText; break; }
                        case "arguments": { arguments = ReadArguments(childnode, arguments); break; }
                        default: break;
                    }
                }
                if (path != "") AddModule("", path, arguments.ToArray());
            }
            var servers = (from XmlNode node in
                               (from XmlNode node in
                                    (from XmlNode node in doc.ChildNodes
                                     where node.NodeType == XmlNodeType.Element && node.Name.ToLower() == "swatter"
                                     select node).First().ChildNodes
                                where node.NodeType == XmlNodeType.Element && node.Name.ToLower() == "servers"
                                select node).First().ChildNodes
                           where node.NodeType == XmlNodeType.Element && node.Name.ToLower() == "server"
                           select node).ToArray();
            foreach (XmlNode servernode in servers)
            {
                string host = "";
                int port = 6667;
                string nick = "";
                string password = "";
                foreach (XmlNode childnode in (from XmlNode node in servernode.ChildNodes where node.NodeType == XmlNodeType.Element select node).ToArray())
                {
                    switch (childnode.Name.ToLower())
                    {
                        case "host": { host = childnode.InnerText; break; }
                        case "port": { port = int.Parse(childnode.InnerText); break; }
                        case "nick": { nick = childnode.InnerText; break; }
                        case "password": { password = childnode.InnerText; break; }
                        default: break;
                    }
                }
                if (host != "" && nick != "")
                {
                    AddServer(host, port, nick, password);
                    var channels = (from XmlNode node in
                                        (from XmlNode node in servernode.ChildNodes
                                         where node.NodeType == XmlNodeType.Element && node.Name.ToLower() == "channels"
                                         select node).First().ChildNodes
                                    where node.NodeType == XmlNodeType.Element && node.Name.ToLower() == "channel"
                                    select node.InnerText).ToArray();
                    foreach (var chan in channels)
                    {
                        AddChannel(host, chan);
                    }
                    modules = (from XmlNode node in
                                   (from XmlNode node in servernode.ChildNodes
                                    where node.NodeType == XmlNodeType.Element && node.Name.ToLower() == "modules"
                                    select node).First().ChildNodes
                               where node.NodeType == XmlNodeType.Element && node.Name.ToLower() == "module"
                               select node).ToArray();
                    foreach (XmlNode modulenode in modules)
                    {
                        string path = "";
                        List<object> arguments = new List<object>();
                        foreach (XmlNode childnode in (from XmlNode node in modulenode.ChildNodes where node.NodeType == XmlNodeType.Element select node).ToArray())
                        {
                            switch (childnode.Name.ToLower())
                            {
                                case "path": { path = childnode.InnerText; break; }
                                case "arguments": { arguments = ReadArguments(childnode, arguments); break; }
                                default: break;
                            }
                        }
                        if (path != "") AddModule(host, path, arguments.ToArray());
                    }
                }
            }
        }
        private static List<object> ReadArguments(XmlNode Node, List<object> Arguments)
        {
            foreach (XmlNode childnode in (from XmlNode node in Node.ChildNodes where node.NodeType == XmlNodeType.Element select node).ToArray())
            {
                switch (childnode.Name.ToLower())
                {
                    case "string": { Arguments.Add(childnode.InnerText); break; }
                    case "int": { Arguments.Add(int.Parse(childnode.InnerText)); break; }
                    case "bool": { Arguments.Add(bool.Parse(childnode.InnerText.ToLower())); break; }
                    case "array": { Arguments.Add(ReadArguments(childnode, new List<object>()).ToArray()); break; }
                    case "pair":
                        {
                            var key = (from XmlNode innernode in childnode.ChildNodes where innernode.Name.ToLower() == "key" select innernode).First().InnerText;
                            var value = (from XmlNode innernode in childnode.ChildNodes where innernode.Name.ToLower() == "value" select innernode).First();
                            if (!(value.ChildNodes.Count > 1))
                            {
                                Arguments.Add(new KeyValuePair<string, string>(key, value.InnerText));
                            }
                            else
                            {
                                Arguments.Add(new KeyValuePair<string, object[]>(key, ReadArguments(value, new List<object>()).ToArray()));
                            }
                            break;
                        }
                    default: break;
                }
            }
            return Arguments;
        }
        #endregion
    }
}
