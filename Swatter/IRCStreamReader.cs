﻿using System;
using System.Collections.Generic;
using System.Net.Sockets;
using System.Text;

namespace Swatter
{
    internal class IRCStreamReader
    {
        #region Variables
        private NetworkStream _stream = null;
        private string _stringbuffer = "";
        private List<string> _commandbuffer = new List<string>();
        #endregion

        #region Constructors
        public IRCStreamReader(NetworkStream Stream)
        {
            _stream = Stream;
        }
        #endregion

        #region Functions
        public string[] PollCommands()
        {
            while (_stream.DataAvailable)
            {
                int result;
                if ((result = _stream.ReadByte()) != -1)
                {
                    char value = Convert.ToChar((byte)result);
                    if (value == '\r' || value == '\n')
                    {
                        if (_stringbuffer != "") _commandbuffer.Add(_stringbuffer);
                        _stringbuffer = "";
                    }
                    else
                    {
                        _stringbuffer += value;
                    }
                }
            }
            var commands = _commandbuffer.ToArray();
            _commandbuffer.Clear();
            return commands;
        }
        #endregion
    }
}
