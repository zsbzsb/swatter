﻿using System;

namespace Swatter
{
    /// <summary>Abstract class for implementing custom IRC bot modules using Swatter</summary>
    public abstract class ModuleBase
    {
        #region Variables
        private ModuleManager _manager = null;
        private string _nickname = "";
        #endregion

        #region Properties
        public abstract Guid ID { get; }
        public abstract string Name { get; }
        public abstract string Description { get; }
        public abstract string Version { get; }
        public string Nickname
        {
            get
            {
                return _nickname;
            }
            internal set
            {
                _nickname = value;
            }
        }
        #endregion

        #region Functions
        internal void SetManager(ModuleManager Manager)
        {
            _manager = Manager;
        }
        public virtual void Initialize() { }
        public virtual void Update(Time DeltaTime) { }
        public virtual void MessageRecieved(MessageData MessageData) { }
        public virtual void IRCCommandRecieved(CommandData CommandData) { }
        public virtual void GlobalCommandRecieved(object ID, string Arguments, MessageData MessageData) { }
        public virtual void NickCommandRecieved(object ID, string Arguments, MessageData MessageData) { }
        protected void SendCommand(string Command)
        {
            _manager.OnSendCommand(this, Command);
        }
        protected void SendMessage(string Target, string Message)
        {
            _manager.OnSendMessage(this, Target, Message);
        }
        protected void RegisterCallback(Delegate Callback, object[] Parameters, Time TimeLimit, bool Loop)
        {
            _manager.OnRegisterCallback(this, Callback, Parameters, TimeLimit, Loop);
        }
        protected void RegisterGlobalCommand(object ID, string Command)
        {
            _manager.OnRegisterGlobalCommand(this, ID, Command);
        }
        protected void RegisterNickCommand(object ID, string Command)
        {
            _manager.OnRegisterNickCommand(this, ID, Command);
        }
        #endregion
    }
}
