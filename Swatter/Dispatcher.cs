﻿using System;
using System.Threading;
using System.Collections.Concurrent;

namespace Swatter
{
    /// <summary>Allows event callbacks to be preformed on the owner's thread.</summary>
    /// <remarks>This is used primary to help syncronize multiple threads.</remarks>
    internal class Dispatcher
    {
        #region Variables
        private static ConcurrentDictionary<int, Dispatcher> _dispatchers = new ConcurrentDictionary<int, Dispatcher>();
        private int _threadid = -1;
        private ConcurrentBag<Tuple<Delegate, object[], bool>> _callbacks = new ConcurrentBag<Tuple<Delegate, object[], bool>>();
        private ConcurrentDictionary<Delegate, object> _callbackreturns = new ConcurrentDictionary<Delegate, object>();
        #endregion

        #region Properties
        /// <summary>Gets the dispatcher for the current thread, if none exist a new one will be created.</summary>
        public static Dispatcher CurrentDispatcher
        {
            get
            {
                int id = Thread.CurrentThread.ManagedThreadId;
                if (_dispatchers.ContainsKey(id)) return _dispatchers[id];
                if (_dispatchers.TryAdd(id, new Dispatcher(id))) return _dispatchers[id];
                else return null;
            }
        }
        #endregion

        #region Constructors
        private Dispatcher(int ThreadID)
        {
            _threadid = ThreadID;
        }
        #endregion

        #region Functions
        /// <summary>Handles all the callbacks that have been queued, can only be called on the owner's thread.</summary>
        public void HandleCallbacks()
        {
            if (Thread.CurrentThread.ManagedThreadId != _threadid) return;
            while (!_callbacks.IsEmpty)
            {
                var callback = new Tuple<Delegate, object[], bool>(null, null, false);
                if (_callbacks.TryTake(out callback))
                {
                    if (callback.Item3)
                    {
                        _callbackreturns.TryAdd(callback.Item1, callback.Item1.DynamicInvoke(callback.Item2));
                    }
                    else callback.Item1.DynamicInvoke(callback.Item2);
                }
            }
        }
        /// <summary>Invokes a blocking callback to the owner's thread.</summary>
        /// <param name="Callback">The delegate to invoke</param>
        public void Invoke(Delegate Callback)
        {
            Invoke(Callback, new object[] { });
        }
        /// <summary>Invokes a blocking callback to the owner's thread.</summary>
        /// <param name="Callback">Delegate to invoke for the callback</param>
        /// <param name="Parameters">Parameter array to pass during the callback</param>
        public void Invoke(Delegate Callback, object[] Parameters)
        {
            Invoke<object>(Callback, Parameters);
        }
        /// <summary>Invokes a blocking callback to the owner's thread.</summary>
        /// <typeparam name="T">Return type of the delegate</typeparam>
        /// <param name="Callback">The delegate to invoke</param>
        public T Invoke<T>(Delegate Callback)
        {
            return Invoke<T>(Callback, new object[] { });
        }
        /// <summary>Invokes a blocking callback to the owner's thread.</summary>
        /// <typeparam name="T">Return type of the delegate</typeparam>
        /// <param name="Callback">The delegate to invoke</param>
        /// <param name="Parameters">Parameter array to pass during the callback</param>
        public T Invoke<T>(Delegate Callback, object[] Parameters)
        {
            if (Thread.CurrentThread.ManagedThreadId != _threadid)
            {
                _callbacks.Add(new Tuple<Delegate, object[], bool>(Callback, Parameters, true));
                while (!_callbackreturns.ContainsKey(Callback)) { Thread.Sleep(5); }
                object retvalue = null;
                _callbackreturns.TryRemove(Callback, out retvalue);
                return (T)retvalue;
            }
            else
            {
                return (T)Callback.DynamicInvoke(Parameters);
            }
        }
        /// <summary>Invokes a non-blocking callback to the owner's thread<./summary>
        /// <param name="Callback">The delegate to invoke</param>
        /// <param name="IgnoreDuplicates">Flag to determine if the callback will be ignored if the delegate is already queued</param>
        public void InvokeAsync(Delegate Callback, bool IgnoreDuplicates = true)
        {
            InvokeAsync(Callback, new object[] { }, IgnoreDuplicates);
        }
        /// <summary>Invokes a non-blocking callback to the owner's thread.</summary>
        /// <param name="Callback">The delegate to invoke</param>
        /// /// <param name="Parameters">Parameter array to pass during the callback</param>
        /// <param name="IgnoreDuplicates">Flag to determine if the callback will be ignored if the delegate is already queued</param>
        public void InvokeAsync(Delegate Callback, object[] Parameters, bool IgnoreDuplicates = false)
        {
            if (Thread.CurrentThread.ManagedThreadId != _threadid)
            {
                if (IgnoreDuplicates)
                {
                    var queuedcalls = _callbacks.ToArray();
                    bool duplicate = false;
                    foreach (var call in queuedcalls)
                    {
                        duplicate = !call.Item3 && call.Item1.Method == Callback.Method && call.Item1.Target == Callback.Target;
                        if (duplicate) break;
                    }
                    if (!duplicate) _callbacks.Add(new Tuple<Delegate, object[], bool>(Callback, Parameters, false));
                }
                else _callbacks.Add(new Tuple<Delegate, object[], bool>(Callback, Parameters, false));
            }
            else
            {
                Callback.DynamicInvoke(Parameters);
            }
        }
        #endregion
    }
}
