﻿using System;
using System.Collections.Generic;
using System.Threading;

namespace Swatter
{
    internal class ServerManager
    {
        #region Variables
        private IRCClient _client = null;
        private ModuleManager _modulemanager = null;
        private List<Tuple<string, object[]>> _modulelist = new List<Tuple<string, object[]>>();
        private Thread _serverthread = null;
        private Dispatcher _dispatcher = Dispatcher.CurrentDispatcher;
        #endregion

        #region Properties
        public string Server
        {
            get
            {
                return _client.Server;
            }
        }
        #endregion

        #region Constructors
        public ServerManager(string Server, int Port, string Nick, string Password)
        {
            _modulemanager = new ModuleManager(Nick);
            _client = new IRCClient(Server, Nick, Password, Port);
            _modulemanager.SendCommand += SendCommand;
            _modulemanager.SendMessage += SendMessage;
            _client.MessageRecieved += MessageRecieved;
            _client.CommandRecieved += CommandRecieved;
            _client.Update += Update;
        }
        #endregion

        #region Functions
        public void AddChannel(string Channel)
        {
            _client.RegisterChannel(Channel);
        }
        public void AddModule(string Path, object[] Arguments)
        {
            _modulelist.Add(new Tuple<string, object[]>(Path, Arguments));
        }
        public void LoadLocalModules()
        {
            foreach (var mod in _modulelist)
            {
                LoadModule(mod.Item1, mod.Item2);
            }
        }
        public void LoadModule(string Path, object[] Arguments)
        {
            var modules = ModuleLoader.LoadModule(Path, Arguments);
            foreach (var mod in modules)
            {
                _modulemanager.RegisterModule(mod);
            }
        }
        public void InitializeModules()
        {
            _modulemanager.InitializeModules();
        }
        private void SendCommand(string Command)
        {
            if (!_client.IsReady) return;
            _client.SendCommand(Command);
        }
        private void SendMessage(string Target, string Message)
        {
            if (!_client.IsReady) return;
            _client.SendMessage(Target, Message);
        }
        private void MessageRecieved(IRCClient Sender, MessageData MessageData)
        {
            _modulemanager.MessageRecieved(MessageData);
        }
        private void CommandRecieved(IRCClient Sender, CommandData CommandData)
        {
            _modulemanager.CommandRecieved(CommandData);
        }
        private void Update(IRCClient Sender, Time DeltaTime)
        {
            _modulemanager.Update(DeltaTime);
        }
        public void StartServer()
        {
            _serverthread = new Thread(new ThreadStart(StartServerThread));
            _serverthread.Start();
        }
        private void StartServerThread()
        {
            try
            {
                while (!_client.Connect())
                {
                    Console.WriteLine("[" + Server + "] Connection failed. Retrying in 5 seconds.");
                    Thread.Sleep(5000);
                }
                Console.WriteLine("[" + Server + "] Connected.");
                _client.Run();
            }
            catch (Exception e)
            {
                _dispatcher.Invoke(new Action(() => { throw e; }));
            }
        }
        #endregion
    }
}
